set -e

gcloud container clusters get-credentials uda --zone europe-west4-a --project sage-wave-767

# go to the main folder first
DIR=deployment
cd ..

docker build -f deployment/Dockerfile -t gcr.io/sage-wave-767/hello-world:latest .

# and go back
cd $DIR

gcloud docker -- push gcr.io/sage-wave-767/hello-world:latest

set +e
kubectl create --save-config -f hello-world.yaml
set -e

kubectl patch deployment hello-world -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"date\":\"`date +'%s'`\"}}}}}"
kubectl apply -f hello-world.yaml