#!/usr/bin/python3
from setuptools import setup

try:
    with open("../VERSION") as f:
        version = f.read().strip()
except FileNotFoundError:
    with open("VERSION") as f:
        version = f.read().strip()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()
    requirements = [r for r in requirements if not r.startswith('-i ')]

if __name__ == '__main__':  # pragma: no cover
    # noinspection SpellCheckingInspection
    setup(
        name='hello_world',
        version=version,
        description='test',
        url='some url',
        author='Shopping Minds',
        author_email='ramon@shoppingminds.com',
        packages=[
            'hello_world'
        ],
        install_requires=requirements,
    )
