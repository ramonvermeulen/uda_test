import falcon

import json
import socket
from cfg import CFG


class HealthResource:
    def on_get(self, req, resp):
        """Returns Health Status"""

        resp.body = json.dumps({"status": "OK VERSION 1", "hostname": socket.gethostname()})


if __name__ == '__main__':
    app = falcon.API()

    # HEALTH Page
    app.add_route('/ping', HealthResource())

    CFG.LOGGER.uda.debug("url mapping loaded")

    from wsgiref import simple_server

    port = 80
    httpd = simple_server.make_server('0.0.0.0', port, app)

    CFG.LOGGER.uda.info(f"start server on port {port}")
    httpd.serve_forever()
